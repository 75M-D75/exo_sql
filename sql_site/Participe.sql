-- phpMyAdmin SQL Dump
-- version OVH
-- https://www.phpmyadmin.net/
--
-- Hôte : montpellxwroot.mysql.db
-- Généré le :  Dim 26 jan. 2020 à 19:46
-- Version du serveur :  5.6.46-log
-- Version de PHP :  7.3.12


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  montpellxwroot
--

-- --------------------------------------------------------

--
-- Structure de la table Participe
--

CREATE TABLE Participe (
  mail_inscrit varchar(100) NOT NULL,
  id_evenement integer NOT NULL
);

--
-- Déchargement des données de la table Participe
--

INSERT INTO Participe (mail_inscrit, id_evenement) VALUES
('mail1@gmail.com', 3),
('mail1@gmail.com', 6),
('mail2@lirmm.fr', 1),
('mail3@supagro.fr', 1),
('mail4@umontpellier.fr', 7),
('mail5@gmail.com', 1),
('mail5@gmail.com', 2),
('mail5@gmail.com', 5),
('mail6@umontpellier.fr', 1),
('mail6@umontpellier.fr', 3),
('mail6@umontpellier.fr', 6),
('mail7@ird.fr', 3),
('mail8@umontpellier.fr', 1),
('mail9@umontpellier.fr', 1),
('mail10@umontpellier.fr', 1),
('mail11@cnrs.fr', 1),
('mail12@gmail.com', 1),
('mail12@gmail.com', 3),
('mail12@gmail.com', 7),
('mail13@umontpellier.fr', 1),
('mail14@ragt.fr', 1),
('mail15@sfr.fr', 3),
('mail16@orange.fr', 1),
('mail16@orange.fr', 4),
('mail16@orange.fr', 5),
('mail17@hotmail.fr', 3),
('mail18@yahoo.de', 1),
('mail18@yahoo.de', 4),
('mail18@yahoo.de', 7),
('mail19@chu-montpellier.fr', 1),
('mail20@cgiar.org', 1),
('mail21@umontpellier.fr', 1),
('mail21@umontpellier.fr', 2),
('mail21@umontpellier.fr', 6),
('mail22@igf.cnrs.fr', 1),
('mail22@igf.cnrs.fr', 2),
('mail22@igf.cnrs.fr', 6),
('mail23@cefe.cnrs.fr', 3),
('mail23@cefe.cnrs.fr', 6),
('mail24@live.fr', 3),
('mail24@live.fr', 7),
('mail25@UMONTPELLIER.FR', 3),
('mail25@UMONTPELLIER.FR', 6),
('mail26@etu.umontpellier.fr', 1),
('mail26@etu.umontpellier.fr', 4),
('mail27@ird.fr', 1),
('mail28@etu.umontpellier.fr', 1),
('mail28@etu.umontpellier.fr', 3),
('mail28@etu.umontpellier.fr', 4),
('mail28@etu.umontpellier.fr', 7),
('mail29@cirad.fr', 1),
('mail29@cirad.fr', 4),
('mail29@cirad.fr', 5),
('mail30@gmail.com', 1),
('mail31@ird.fr', 1),
('mail32@live.fr', 1),
('mail32@live.fr', 3),
('mail32@live.fr', 6),
('mail33@ird.fr', 1),
('mail34@igh.cnrs.fr', 1),
('mail34@igh.cnrs.fr', 2),
('mail34@igh.cnrs.fr', 5),
('mail35@gmail.com', 3),
('mail36@gmail.com', 1),
('mail36@gmail.com', 2),
('mail36@gmail.com', 6),
('mail37@gmail.com', 1),
('mail37@gmail.com', 4),
('mail37@gmail.com', 7),
('mail38@ird.fr', 1),
('mail39@umontpellier.fr', 1),
('mail39@umontpellier.fr', 5),
('mail40@nanoporetech.com', 1),
('mail41@etu.umontpellier.fr', 1),
('mail41@etu.umontpellier.fr', 3),
('mail41@etu.umontpellier.fr', 5),
('mail42@gmail.com', 3),
('mail43@gmail.com', 1),
('mail43@gmail.com', 3),
('mail43@gmail.com', 7),
('mail44@ird.fr', 1),
('mail45@live.fr', 1),
('mail45@live.fr', 2),
('mail45@live.fr', 6),
('mail46@ird.fr', 1),
('mail47@etu.umontpellier.fr', 1),
('mail47@etu.umontpellier.fr', 2),
('mail47@etu.umontpellier.fr', 5),
('mail48@umontpellier.fr', 1),
('mail49@ird.fr', 1),
('mail50@gmail.com', 1),
('mail51@gmail.com', 1),
('mail51@gmail.com', 3),
('mail51@gmail.com', 7),
('mail52@cirad.fr', 1),
('mail52@cirad.fr', 4),
('mail52@cirad.fr', 7),
('mail53@umontpellier.fr', 1),
('mail53@umontpellier.fr', 4),
('mail53@umontpellier.fr', 6),
('mail54@igh.cnrs.fr', 1),
('mail55@cgiar.org', 1),
('mail56@hotmail.fr', 1),
('mail56@hotmail.fr', 3),
('mail56@hotmail.fr', 6),
('mail57@gmail.com', 3),
('mail58@inra.fr', 1),
('mail58@inra.fr', 3),
('mail58@inra.fr', 6),
('mail59@gmail.com', 1),
('mail60@umontpellier.fr', 1),
('mail61@umontpellier.fr', 1),
('mail62@ird.fr', 3),
('mail62@ird.fr', 6),
('mail63@univ-rouen.fr', 1),
('mail63@univ-rouen.fr', 4),
('mail63@univ-rouen.fr', 6),
('mail64@hotmail.fr', 1),
('mail64@hotmail.fr', 3),
('mail64@hotmail.fr', 6),
('mail65@etu.umontpellier.fr', 1),
('mail65@etu.umontpellier.fr', 3),
('mail65@etu.umontpellier.fr', 6),
('mail66@inserm.fr', 1),
('mail67@etu.umontpellier.fr', 1),
('mail68@ird.fr', 1),
('mail68@ird.fr', 3),
('mail68@ird.fr', 7),
('mail69@cnrs.fr', 1),
('mail70@yahoo.fr', 1),
('mail71@igmm.cnrs.fr', 1),
('mail72@umontpellier.fr', 1),

--
-- Index pour les tables déchargées
--

--
-- Index pour la table Participe
--
ALTER TABLE Participe
  ADD PRIMARY KEY (mail_inscrit,id_evenement),
  ADD KEY Participe_ibfk_2 (id_evenement);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table Participe
--
ALTER TABLE Participe
  ADD CONSTRAINT Participe_ibfk_1 FOREIGN KEY (mail_inscrit) REFERENCES Inscrit (mail) ON DELETE CASCADE,
  ADD CONSTRAINT Participe_ibfk_2 FOREIGN KEY (id_evenement) REFERENCES Evenement (ID);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
