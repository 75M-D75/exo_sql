-- phpMyAdmin SQL Dump
-- version OVH
-- https://www.phpmyadmin.net/
--
-- Hôte : montpellxwroot.mysql.db
-- Genere le :  Dim 26 jan. 2020 à 19:46
-- Version du serveur :  5.6.46-log
-- Version de PHP :  7.3.12

/*!40101 SET mail1@OLD_CHARACTER_SET_CLIENT=mail1@mail1@CHARACTER_SET_CLIENT */;
/*!40101 SET mail1@OLD_CHARACTER_SET_RESULTS=mail1@mail1@CHARACTER_SET_RESULTS */;
/*!40101 SET mail1@OLD_COLLATION_CONNECTION=mail1@mail1@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de donnees :  montpellxwroot
--

-- --------------------------------------------------------

--
-- Structure de la table Inscrit
-

--
-- Dechargement des donnees de la table Inscrit
--

INSERT INTO Inscrit (nom, prenom, mail, statut, droit_image, civilite, abstract, pays, affiliation) VALUES
('Name1', 'Pname1', 'mail1@gmail.com', 'etudiant', 0, 'Madame', '', 'France', 'ISEM'),
('Name2', 'Pname2', 'mail2@lirmm.fr', 'autre', 1, 'Monsieur', '', 'France', 'LIRMM'),
('Name3', 'Pname3', 'mail3@supagro.fr', 'ingenieur', 1, 'Monsieur', '', 'France', 'BPMP'),
('Name4', 'Pname4', 'mail4@umontpellier.fr', 'post-doctorant', 1, 'Monsieur', '', 'France', 'ISEM'),
('Name5', 'Pname5', 'mail5@gmail.com', 'docteur', 1, 'Monsieur', '', 'France', 'Universite Clermont-Auvergne'),
('Name6', 'Pname6', 'mail6@umontpellier.fr', 'ingenieur', 0, 'Madame', '', 'France', 'Institut des sciences de l evolution (ISEM)'),
('Name7', 'Pname7', 'mail7@ird.fr', 'docteur', 0, 'Madame', '', 'France', 'UMR ISEM'),
('Name8', 'Pname8', 'mail8@umontpellier.fr', 'enseignant_chercheur', 1, 'Madame', '', 'France', NULL),
('Name9', 'Pname9', 'mail9@umontpellier.fr', 'enseignant_chercheur', 1, 'Madame', '', 'France', 'UM LIRMM'),
('Name10', 'Pname10', 'mail10@umontpellier.fr', 'enseignant_chercheur', 1, 'Monsieur', '', 'France', 'LPHI'),
('Name11', 'Pname11', 'mail11@cnrs.fr', 'enseignant_chercheur', 1, 'Monsieur', '', 'France', 'BPMP'),
('Name12', 'Pname12', 'mail12@gmail.com', 'etudiant', 1, 'Monsieur', '', 'France', 'M1 BCD'),
('Name13', 'Pname13', 'mail13@umontpellier.fr', 'post-doctorant', 1, 'Madame', '', 'France', 'IGH'),
('Name14', 'Pname14', 'mail14@ragt.fr', 'ingenieur', 0, 'Monsieur', '', 'France', 'RAGT 2n'),
('Name15', 'Pname15', 'mail15@sfr.fr', 'etudiant', 1, 'Monsieur', '', 'France', 'Universite de Montpellier'),
('Name16', 'Pname16', 'mail16@orange.fr', '', 1, '', '', 'France', ''),
('Name17', 'Pname17', 'mail17@hotmail.fr', 'etudiant', 1, 'Madame', '', 'France', 'universite de montpellier'),
('Name18', 'Pname18', 'mail18@yahoo.de', 'autre', 1, 'Madame', '', 'France', ''),
('Name19', 'Pname19', 'mail19@chu-montpellier.fr', 'ingenieur', 1, 'Monsieur', '', 'France', 'CHU Montpellier'),
('Name20', 'Pname20', 'mail20@cgiar.org', 'ingenieur', 1, 'Madame', '', 'France', 'Bioversity et ISEM'),
('Name21', 'Pname21', 'mail21@umontpellier.fr', 'ingenieur', 0, 'Madame', '', 'France', 'LPHI UMR5235'),
('Name22', 'Pname22', 'mail22@igf.cnrs.fr', 'etudiant', 1, 'Monsieur', '', 'France', ''),
('Name23', 'Pname23', 'mail23@cefe.cnrs.fr', 'docteur', 0, 'Madame', '', 'France', 'CEFE'),
('Name24', 'Pname24', 'mail24@live.fr', 'etudiant', 0, 'Madame', '', 'France', ''),
('Name25', 'Pname25', 'mail25@UMONTPELLIER.FR', 'autre', 0, '', '', 'Afghanistan', 'UMR 5554 CNRS-UM/ ISEM'),
('Name26', 'Pname26', 'mail26@etu.umontpellier.fr', 'etudiant', 1, 'Madame', '', 'France', 'UM'),
('Name27', 'Pname27', 'mail27@ird.fr', 'docteur', 1, 'Madame', '', 'France', 'IRD'),
('Name28', 'Pname28', 'mail28@etu.umontpellier.fr', 'etudiant', 1, 'Madame', '', 'France', 'UM'),
('Name29', 'Pname11', 'mail29@cirad.fr', 'autre', 1, 'Monsieur', '', 'France', 'UMR AGAP - CIRAD'),
('Name30', 'Pname10', 'mail30@gmail.com', 'ingenieur', 1, 'Madame', '', 'France', 'CIRAD'),
('Name31', 'Pname17', 'mail31@ird.fr', 'docteur', 1, 'Monsieur', '', 'France', 'IRD Montpellier'),
('Name32', 'Pname7', 'mail32@live.fr', 'etudiant', 1, 'Madame', '', 'France', 'Faculte des Sciences de Montpellier'),
('Name33', 'Pname7', 'mail33@ird.fr', 'post-doctorant', 1, 'Madame', '', 'France', 'IRD/ISEM'),
('Name34', 'Pname1', 'mail34@igh.cnrs.fr', 'docteur', 1, 'Monsieur', '', 'France', 'Institut de Genetique Humaine - Laboratoire STI'),
('Name35', 'Pname7', 'mail35@gmail.com', 'etudiant', 0, 'Madame', '', 'France', 'Faculte des Sciences'),
('Name36', 'Pname4', 'mail36@gmail.com', 'etudiant', 1, 'Madame', '', 'France', 'MASTER 2'),
('Name37', 'Pname8', 'mail37@gmail.com', 'etudiant', 1, 'Madame', '', 'France', 'U montpellier '),
('Name38', 'Pname7', 'mail38@ird.fr', 'docteur', 1, 'Madame', '', 'France', 'Mivegec (CNRS) '),
('Name39', 'Pname4', 'mail39@umontpellier.fr', 'ingenieur', 1, 'Monsieur', '', 'France', 'CNRS/Institut Monptellierain Alexander Grothendieck'),
('Name40', 'Pname1', 'mail40@nanoporetech.com', 'prive', 0, 'Monsieur', '', 'France', NULL),
('Name41', 'Pname1', 'mail41@etu.umontpellier.fr', 'etudiant', 0, 'Monsieur', '', 'France', ''),
('Name42', 'Pname7', 'mail42@gmail.com', 'etudiant', 1, 'Madame', '', 'France', 'M2 SSV'),
('Name43', 'Pname5', 'mail43@gmail.com', 'prive', 1, 'Madame', '', 'France', 'Servier'),
('Name44', 'Pname1', 'mail44@ird.fr', 'docteur', 1, 'Madame', '', 'France', NULL),
('Name45', 'Pname55', 'mail45@live.fr', 'autre', 1, 'Madame', '', 'France', 'ex-etudiante biologie sante'),
('Name46', 'Pname46', 'mail46@ird.fr', 'enseignant_chercheur', 1, 'Monsieur', '', 'France', 'ISEM'),
('Name47', 'Pname15', 'mail47@etu.umontpellier.fr', 'etudiant', 0, 'Madame', '', 'France', 'fac de science'),
('Name48', 'Pname22', 'mail48@umontpellier.fr', 'enseignant_chercheur', 1, 'Monsieur', '', 'France', 'DSIN'),
('Name49', 'Pname1', 'mail49@ird.fr', 'ingenieur', 0, 'Madame', '', 'France', 'IRD'),
('Name50', 'Pname1', 'mail50@gmail.com', 'ingenieur', 1, 'Monsieur', '', 'France', 'GHFC, Pasteur +  JeBiF + SFBI'),
('Name51', 'Pname1', 'mail51@gmail.com', 'etudiant', 1, 'Monsieur', '', 'France', 'PRISM'),
('Name52', 'Pname12', 'mail52@cirad.fr', 'autre', 1, 'Madame', '', 'France', 'Cirad'),
('Name53', 'Pname1', 'mail53@umontpellier.fr', 'ingenieur', 1, 'Monsieur', '', 'France', 'UMR MIVEGEC'),
('Name54', 'Pname1', 'mail54@igh.cnrs.fr', 'docteur', 0, 'Madame', '', 'France', ''),
('Name55', 'Pname5', 'mail55@cgiar.org', 'autre', 1, 'Monsieur', '', 'France', 'Alliance Bioversity/CIAT'),
('Name56', 'Pname1', 'mail56@hotmail.fr', 'etudiant', 0, 'Madame', '', 'France', 'Faculte des sciences de Montpellier'),
('Name57', 'Pname4', 'mail57@gmail.com', 'etudiant', 1, 'Madame', '', 'France', 'M2 SSV'),
('Name58', 'Pname1', 'mail58@inra.fr', 'autre', 0, 'Monsieur', '', 'France', 'LSTM INRA'),
('Name59', 'Pname1', 'mail59@gmail.com', 'docteur', 1, 'Monsieur', '', 'France', 'LPHI'),
('Name60', 'Pname2', 'mail60@umontpellier.fr', 'docteur', 0, 'Madame', '', 'France', 'ISEM (Institut des Sciences de l evolution de Montpellier)'),
('Name61', 'Pname1', 'mail61@umontpellier.fr', 'ingenieur', 1, 'Madame', '', 'France', 'CNRS'),
('Name62', 'Pname1', 'mail62@ird.fr', 'docteur', 0, 'Madame', '', 'France', 'MIVEGEC'),
('Name63', 'Pname2', 'mail63@univ-rouen.fr', 'ingenieur', 1, 'Madame', '', 'France', 'GRAM EA2656'),
('Name64', 'Pname1', 'mail64@hotmail.fr', 'etudiant', 1, 'Madame', '', 'France', 'ISEM'),
('Name65', 'Pname55', 'mail65@etu.umontpellier.fr', 'etudiant', 1, 'Madame', '', 'France', 'UM'),
('Name66', 'Pname11', 'mail66@inserm.fr', 'docteur', 1, 'Madame', '', 'France', 'IRCM/LPHI'),
('Name67', 'Pname67', 'mail67@etu.umontpellier.fr', 'etudiant', 1, 'Monsieur', '', 'France', ''),
('Name68', 'Pname15', 'mail68@ird.fr', 'enseignant_chercheur', 1, 'Monsieur', '', 'France', 'IRD - DIADE'),
('Name69', 'Pname5', 'mail69@cnrs.fr', 'enseignant_chercheur', 1, 'Madame', '', 'France', 'MIVEGEC - CNRS'),
('Name70', 'Pname1', 'mail70@yahoo.fr', 'ingenieur', 1, 'Madame', '', 'France', ''),
('Name71', 'Pname17', 'mail71@igmm.cnrs.fr', 'ingenieur', 1, 'Monsieur', '', 'France', 'IGMM CNRS'),
('Name72', 'Pname4', 'mail72@umontpellier.fr', 'docteur', 1, 'Madame', '', 'France', '');

-- Index pour les tables dechargees
--

--
-- Index pour la table Inscrit
--
ALTER TABLE Inscrit
  ADD PRIMARY KEY (mail);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=mail1@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=mail1@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=mail1@OLD_COLLATION_CONNECTION */;
