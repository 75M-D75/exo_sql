 $(document).ready(function () {

    /*  Disables mobile keyboard from displaying when clicking +/- inputs */

    $('.input-number-decrement').attr('readonly', 'readonly');
    $('.input-number-increment').attr('readonly', 'readonly');

    /*Attributes variables with min and max values for counter*/

    var min = $(".input-number-decrement").data("min");
    var max = $(".input-number-increment").data("max");

    /*Incrementally increases the value of the counter up to max value, and ensures +/- input works when input has no value (i.e. when the input-number field has been cleared) */

    $(".input-number-increment").on('click', function () {
        var $incdec = $(this).prev();

        if ($incdec.val() == '') {
            $incdec.val(1);
        } 
        else if ($incdec.val() < max) {
            $incdec.val(parseInt($incdec.val()) + 1);
        }
    });

    /*Incrementally decreases the value of the counter down to min value, and ensures +/- input works when input has no value (i.e. when the input-number field has been cleared) */

    $(".input-number-decrement").on('click', function () {
        var $incdec = $(this).next();

        if ($incdec.val() == '') {
            $incdec.val(0);
        } 
        else if ($incdec.val() > min) {
            $incdec.val(parseInt($incdec.val()) - 1);
        }
    });

    /* Removes any character other than a number that is entered in number input */

    var input = document.getElementsByClassName('input-number');

    $(input).on('keyup input', function () {

        this.value = this.value.replace(/[^0-9]/g, '');

        /* Gives an error if number entered is over max */

        if (this.value > max) {
            this.value = max;
            error();
        }
        else if(this.value < min){
            this.value = min;
            error();
        }
    });

    /* Function to display error for numbers over max */

    function error() {
        $('.data').empty();
        $('.data').append('<p class="error">You can only order between ' + min + ' and ' + max + ' items!</p>');
        $(".data").fadeIn(400);

        /* Fades out error once the counter is clicked on */

        $(".counter").on('click', function () {
            $(".error").fadeOut(800);
        });
    }

});

/*Function to determine what is displayed when form is submitted*/

function submitbutton() {

    $('.data').hide();
    $('.data').empty();

    /* Attributes material order values to variables and sets up variables for singular/plural names */
    var standard = $('#standard').val();
    var archive = $('#archive').val();
    var tape = $('#tape').val();
    var s_name = "";
    var a_name = "";
    var t_name = "";

    /* Sets number input value to 0 if no value is present when submit button is clicked. Also, determines if a singular or plural name will be used */

    if (standard === '') {
        standard = 0;
        s_name = "Standard Boxes";
    } else if (standard == 1) {
        s_name = "Standard Box";
    } else {
        s_name = "Standard Boxes";
    }

    if (archive === '') {
        archive = 0;
        a_name = "Archive Boxes";
    } else if (archive == 1) {
        a_name = "Archive Box";
    } else {
        a_name = "Archive Boxes";
    }

    if (tape === '') {
        tape = 0;
        t_name = "Packing Tapes";
    } else if (tape == 1) {
        t_name = "Packing Tape";
    } else {
        t_name = "Packing Tapes";
    }

    /* Displays error if all values are 0, or displays order confirmation if order is valid */

    if (standard == 0 && archive == 0 && tape == 0) {
        $('.data').append('<p class="error">You have not selected any items!</p>');
        $(".data").fadeIn(400);
    } 
    else {
        var standard = '<p><span id="confirm-num">' + standard + "</span> x " + s_name + "</p>";
        var archive = '<p><span id="confirm-num">' + archive + "</span> x " + a_name + "</p>";
        var tape = '<p><span id="confirm-num">' + tape + "</span> x " + t_name + "</p>";

        var options = {
            direction: "right"
        };
        var effect = 'slide';
        var duration = 800;

        $('.data').append("<h2>Thank you! The following order has been processed:</h2>");
        $('.data').append(standard);
        $('.data').append(archive);
        $('.data').append(tape);
        $(".data").slideDown(500);
        $("#order")[0].reset();
    }
}

function resetbutton() {
    $('.data').fadeOut(400);
}