function choose_active(name) {
	//supprime la classe active de la balise "li" qui est activé (par default Accueil)
    $("li[class=\"active\"]").removeClass("active");
    //active la balise "li" qui est le parent de la balise "a" qui contient le nom passer en paramettre (exemple : liste_statique.php)
    $("a[href=\""+name+"\"]").parents("li").addClass("active");
}