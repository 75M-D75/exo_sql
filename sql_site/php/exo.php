<?php
	// Connexion, sélection de la base de données
	$dbconn = pg_connect("host=localhost port=5432 dbname=plants user=postgres password=MDFXE755") or die('Connexion impossible : ' . pg_last_error());

	$tab_quartier = array();
	$indice_quartier = 0;
	$t = array();
	if( array_key_exists('number', $_REQUEST) ){
		$nb = (int)$_REQUEST['number'];
		$query = "";
		switch ($nb) {
			case 1:
				$query = "SELECT * FROM PLANT;";
				break;
			case 2:
				$query = "SELECT * FROM PLANT;";
				break;
			default:
				# code...
				break;
		}
		
		try {
			$result = pg_query($query);
				
			while($ligne = pg_fetch_array($result, null, PGSQL_ASSOC)){
				$indice_col = 0;
			    foreach ($ligne as $col_value) {
			        $tab_quartier[$indice_quartier][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_quartier = $indice_quartier + 1;
			}
		} 
		catch (Exception $e) {
		    echo 'Exception reçue : ',  $e->getMessage(), "\n";
		}
		
		$t['info'] = $tab_quartier;
		
		// Libère le résultat
		pg_free_result($result);
	}
	echo json_encode($t);

	// Ferme la connexion
	pg_close($dbconn);
?>