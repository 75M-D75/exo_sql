<?php
	//ini_set('display_errors', 'off');
	include '../vars.php';
	include 'function.php';

	$dbh = new PDO("mysql:host=$HOST_DB;dbname=$NAME_DB", $USER_DB, $PASSWORD_DB);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$tab   = array();
	$tab_2 = array();
	$indice_quartier = 0;
	$t = array();
	$bool = false;
	if( array_key_exists('query', $_REQUEST) ){
		$q = $_REQUEST['query'];

		try {
			$result = $dbh->prepare($q);
			if (!$result) {
			    $t['error'] = $dbh->errorInfo();
			    exit();
			}

			$bool = $result->execute();
			if (!$bool) {
				$t['error'] = $dbh->errorInfo();
			}
			
			$taille = $result->columnCount();

			for ($i=0; $i < $taille; $i++) { 
				$meta = $result->getColumnMeta($i);
				$tab[$indice_quartier][$i] = $meta["name"];
			}
			$indice_quartier = $indice_quartier + 1;
			
			while($ligne = $result->fetch(PDO::FETCH_ASSOC)){
				$indice_col = 0;
			    foreach ($ligne as $col_value) {
			        $tab[$indice_quartier][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_quartier = $indice_quartier + 1;
			}

			$t['info'] = $tab;
		
			// Libère le résultat
			$result->closeCursor();
		} 
		catch (PDOException $e) {
		    //echo 'Exception reçue : ',  $e->getMessage(), "\n";
		    $t['error'] = utf8_encode($e->getMessage());
		    
		}
	}

	$indice_quartier = 0;
	if( array_key_exists('number', $_REQUEST) && $bool){
		$nb = (int)$_REQUEST['number'];
		$query = "";
		
		$query = reponse($nb);
		
		try {
			$result  = $dbh->prepare($query);

			$result->execute();
			$taille = $result->columnCount();

			for ($i=0; $i < $taille; $i++) { 
				$meta = $result->getColumnMeta($i);
				$tab_2[$indice_quartier][$i] = $meta["name"];
			}
			$indice_quartier = $indice_quartier + 1;
				
			while($ligne = $result->fetch(PDO::FETCH_ASSOC)){
				$indice_col = 0;
			    foreach ($ligne as $col_value) {
			        $tab_2[$indice_quartier][$indice_col] = $col_value;
			        $indice_col = $indice_col + 1;
			    }
			    $indice_quartier = $indice_quartier + 1;
			}
		} 
		catch (PDOException $e) {
		    //echo 'Exception reçue : ',  $e->getMessage(), "\n";
		}

		$t['soluce'] = $tab_2;

		$diff   = array_diff($t['info'][0], $t['soluce'][0]);
		$diff_2 = array_diff($t['soluce'][0], $t['info'][0]);
		$same = true;
		//
		if(count($diff) == 0 && count($diff_2) == 0 && count($t['info']) == count($t['soluce']) ){
			$size = count($t['info']);
			for ($i=1; $i < $size && $same; $i++) { 
				$diff   = array_diff($t['info'][$i], $t['soluce'][$i]);
				$diff_2 = array_diff($t['soluce'][$i], $t['info'][$i]);

				if( !(count($diff) == 0 && count($diff_2) == 0) ){
					$same = false;
				}
			}
		}
		
		if(count($diff) == 0 && count($diff_2) == 0 && count($t['info']) == count($t['soluce']) && $same){
			$t['same'] = true;
		}
		else{
			$t['same'] = false;
		}
			
		// Libère le résultat
		$result->closeCursor();
	}

	if ( array_key_exists('soluce', $_REQUEST) ) {
		$num = $_REQUEST["soluce"];
		$t["reponse"] = reponse($num);
	}

	echo json_encode($t);
?>


<?php
	// ini_set('display_errors', 'off');
	// // Connexion, sélection de la base de données
	// $dbconn = pg_connect("host=localhost port=5432 dbname=plants user=postgres password=MDFXE755") or die('Connexion impossible : ' . pg_last_error());

	// $tab = array();
	// $tab_2 = array();
	// $indice_quartier = 0;
	// $t = array();

	// if( array_key_exists('query', $_REQUEST) ){
	// 	$query = $_REQUEST['query'];

	// 	try {
	// 		$result = pg_query($query);
	// 		if($result === false){
	// 			$t['error'] = pg_last_error($dbconn);
	// 		}
	// 		$taille = pg_num_fields($result);

	// 		for ($i=0; $i < $taille; $i++) { 
	// 			$tab[$indice_quartier][$i] = pg_field_name ($result, $i );
	// 		}
	// 		$indice_quartier = $indice_quartier + 1;
			
	// 		while($ligne = pg_fetch_array($result, null, PGSQL_ASSOC)){
	// 			$indice_col = 0;
	// 		    foreach ($ligne as $col_value) {
	// 		        $tab[$indice_quartier][$indice_col] = $col_value;
	// 		        $indice_col = $indice_col + 1;
	// 		    }
	// 		    $indice_quartier = $indice_quartier + 1;
	// 		}
	// 	} 
	// 	catch (Exception $e) {
	// 	    echo 'Exception reçue : ',  $e->getMessage(), "\n";
	// 	}
		
	// 	$t['info'] = $tab;
		
	// 	// Libère le résultat
	// 	pg_free_result($result);
	// }

	// $indice_quartier = 0;
	// if( array_key_exists('number', $_REQUEST) ){
	// 	$nb = (int)$_REQUEST['number'];
	// 	$query = "";
	// 	switch ($nb) {
	// 		case 1:
	// 			$query = "SELECT nom, prenom, mail, affiliation FROM INSCRIT;";
	// 			break;
	// 		case 2:
	// 			$query = "SELECT * FROM INSCRIT where droit_image=0;";
	// 			break;
	// 		case 3:
	// 			$query = "SELECT * FROM Participe where id_evenement<4;";
	// 			break;
	// 		case 4:
	// 			$query = "SELECT * FROM Participe where id_evenement>=2 and id_evenement<=4;";
	// 			break;
	// 		case 5:
	// 			$query = "SELECT * FROM Participe where id_evenement=2 or id_evenement=4;";
	// 			break;
	// 		case 6:
	// 			$query = "SELECT Count(*) as nb_participant FROM Participe where id_evenement=6;";
	// 			break;
	// 		case 7:
	// 			$query = "SELECT id_evenement, Count(id_evenement) as nb_participant FROM Participe group by id_evenement;";
	// 			break;
	// 		case 8:
	// 			$query = "SELECT id_evenement, nom, prenom, mail FROM Participe, Inscrit where mail=mail_inscrit ;";
	// 			break;
	// 		case 9:
	// 			$query = "SELECT nom, prenom, mail, id_evenement, id_e2 FROM Participe, Inscrit, (SELECT mail_inscrit, id_evenement as id_e2 from participe) as Participe_6 where mail=Participe.mail_inscrit and mail=Participe_6.mail_inscrit and id_evenement=1 and id_e2=6;";
	// 			break;
	// 		case 10:
	// 			$query = "SELECT nom, prenom, mail, id_evenement, id_e2 FROM Participe, Inscrit, (SELECT mail_inscrit, id_evenement as id_e2 from participe) as Participe_6 where mail=Participe.mail_inscrit and mail=Participe_6.mail_inscrit and id_evenement=1 and id_e2=6;";
	// 			break;
	// 		default:
	// 			# code...
	// 			break;
	// 	}
		
	// 	try {
	// 		$result = pg_query($query);
	// 		$taille = pg_num_fields($result);

	// 		for ($i=0; $i < $taille; $i++) { 
	// 			$tab_2[$indice_quartier][$i] = pg_field_name ($result, $i );
	// 		}
	// 		$indice_quartier = $indice_quartier + 1;
				
	// 		while($ligne = pg_fetch_array($result, null, PGSQL_ASSOC)){
	// 			$indice_col = 0;
	// 		    foreach ($ligne as $col_value) {
	// 		        $tab_2[$indice_quartier][$indice_col] = $col_value;
	// 		        $indice_col = $indice_col + 1;
	// 		    }
	// 		    $indice_quartier = $indice_quartier + 1;
	// 		}
	// 	} 
	// 	catch (Exception $e) {
	// 	    echo 'Exception reçue : ',  $e->getMessage(), "\n";
	// 	}

	// 	$t['soluce'] = $tab_2;

	// 	$diff   = array_diff($t['info'][0], $t['soluce'][0]);
	// 	$diff_2 = array_diff($t['soluce'][0], $t['info'][0]);
	// 	$same = true;
	// 	//
	// 	if(count($diff) == 0 && count($diff_2) == 0 && count($t['info']) == count($t['soluce']) ){
	// 		$size = count($t['info']);
	// 		for ($i=1; $i < $size && $same; $i++) { 
	// 			$diff   = array_diff($t['info'][0], $t['soluce'][0]);
	// 			$diff_2 = array_diff($t['soluce'][0], $t['info'][0]);

	// 			if( !(count($diff) == 0 && count($diff_2) == 0) ){
	// 				$same = false;
	// 			}
	// 		}
	// 	}
		
	// 	// var_dump($t['soluce'][0]);
	// 	// var_dump($t['info'][0]);
	// 	// var_dump($diff);



		
	// 	if(count($diff) == 0 && count($diff_2) == 0 && count($t['info']) == count($t['soluce']) && $same){
	// 		$t['same'] = true;
	// 	}
	// 	else{
	// 		$t['same'] = false;
	// 	}
			
	// 	// Libère le résultat
	// 	// pg_free_result($result);
	// }

	// echo json_encode($t);

	// Ferme la connexion
	// pg_close($dbconn);
?>