<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Requetage">
    <meta name="author" content="M-D">
    <meta name="viewport" content="width=device-width">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
    <meta property="og:site_name" content="Requete" /> <!-- website name -->
    <meta property="og:site" content="" /> <!-- website link -->
    <meta property="og:title" content="Requete"/> <!-- title shown in the actual shared post -->
    <meta property="og:description" content="Requetage" /> <!-- description shown in the actual shared post -->
    <meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
    <meta property="og:url" content="" /> <!-- where do you want your post to link to -->


    <!-- Website Title -->
    <title>Requete</title>
    
    <!-- Styles -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- color -->
    <link id="changeable-colors" rel="stylesheet" href="css/colors/orange.css" />
    

    <!-- style formuliare et bouton -->
    <link rel="stylesheet" type="text/css" href="css/number_input.css">
    <link rel="stylesheet" type="text/css" href="css/file_area.css">
    <link rel="stylesheet" type="text/css" href="css/form_add.css">
    <link rel="stylesheet" type="text/css" href="css/slider.css">
    <!-- <link rel="stylesheet" type="text/css" href="css/my_css.css"> -->

    <link href="css/table.css" rel="stylesheet">
    
    <!-- style de la liste des carte de recete -->
    <link rel="stylesheet" href="css/list_div.css">
    <link href="css/my_css.css" rel="stylesheet">

    <!-- Modernizer -->
    <script src="js/modernizer.js"></script>

    <!-- JS -->
    <script type="text/javascript" src="js/jquery.min.js"></script>

    <script type="text/javascript" src="js/all_function.js"></script>

    <style type="text/css">
        .outer-div{
            margin-top: 50px;
            width: 100%;
        }
        .card input[type='checkbox'] {
          display: inherit;
        }
    </style>

</head>


<body >
    
    <?php
        include("php/header.php");
    ?>

    <!-- va contenire la liste à afficher -->
    


    <div class="container_formulaire">
        <input class="variation" id="pinkaru" type="radio" value="5" name="color" checked="checked"/>

        <!-- main principale -->
        <form id="form_query">

            <section class="typography">
                <h6>ty</h6>
                <div class="font-example">
                  <div class="font-example__bodytext">
                    <h6 class="subheader">TABLES</h6>

                    <p>Inscrit (nom, prenom, <b>mail</b>, statut, droit_image, civilite, abstract, pays, affiliation)
                        <br>
                        Evenement (<b>id</b>, nom, moment, type)
                        <br>
                        Participe (<b>#mail_inscrit</b> (mail → Inscrit), <b>#id_evenement</b> (id → Evenement))
                    </p>

                    <h6 id="num_exo" class="subheader">EXO 1</h6>
                    <p>Selectionner tous les inscrits en affichant nom, prenom, mail, affiliation</p>
                    <h6 class="subheader">Exemple</h6>
                    <p id="example_requete" ></p>
                    <a id="link_doc" href="" target="_blank">Documentation lien</a>
                    <h6 class="subheader">SQL COLOR</h6>
                    <p id="sql" ></p>
                  </div>
                </div>
            </section>

            

            <input id="nb" type="text" name="number" value="1" style="display: none;">

            <section class="inputs-selects">
            <h6>Entrée votre requete</h6>
            <div class="textarea-select">
                <div class="input__wrapper">
                    <label class="label-form">Requete</label>
                    <textarea id="query" name="query" type="text" placeholder="SELECT * FROM..." maxlength="600" required="required"></textarea>
                </div>

                <input type="submit" class="button accent2" value="Executer" />

                
                <!-- <section class="buttons-wrapper"> -->
                    <!-- <h6>Bs</h6> -->
                    <div class="buttons">
                        <!-- <a id="pred_btn" class="button">Precedant</a> -->
                        <a id="pred_btn" class="button">Precedent</a>
                        <a id="next_btn" class="button">Suivant</a>
                    </div>
                <!-- </section> -->
                <div class="buttons">
                    <input type="button" class="button disabled" value="" />
                    <input id="solution" type="button" class="button accent2" value="Solution" />
                </div>
            </div>       
          </section>



          <section class="alerts">
            <!-- <h6>Alerts</h6> -->
            <!-- <div class="alert status-primary">A normal alert here.</div> -->
            <!-- <div class="alert status-secondary">A normal alert here.</div> -->
            <!-- <div class="alert status-info">A normal alert here.</div> -->
            <!-- <div class="alert status-success">Bien joué</div> -->
            <!-- <div class="alert status-error">A sad alert here.</div> -->
          </section>

          <!-- <section class="tooltips">
            <h6>Tooltips</h6>
            <div>
              <div class="tooltip top hovered" data-tooltip="Show a helpful tooltip here 😄"> <span>Light Tooltip</span></div>
              <div class="tooltip top dark hovered" data-tooltip="Show a helpful tooltip here 😄"> <span>Dark Tooltip</span></div>
              <div class="tooltip bottom" data-tooltip="Hello 👋"> <span>Bottom Tooltip</span></div>
            </div>
          </section> -->
        </form>
    </div>

    <div class="container_list">

    </div>


    <!-- Scripts -->
    <script type="text/javascript">

        $("body").css("background", "#2a313b");

    </script>


    <script type="text/javascript" src="js/number_btn.js"></script>

    <!-- ALL JS FILES -->
    <script src="js/all.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/custom.js"></script>

    <script type="text/javascript">

        //Lors de l'ajout d'une nouvelle recette via le formulaire
        $('#form_query').on('submit', function(e) {
            e.preventDefault();
            console.log($('#form_query').serialize());
            case_word_key();
            $.post("php/query.php", $('#form_query').serialize(), function(data) {
                data = JSON.parse(data);
                console.log(data);

                if(data.same){
                    $(".alerts").html('<div class="alert status-success">Bien joué 😄</div>');
                }
                else{
                    $(".alerts").html('<div class="alert status-error">Reesayer encore</div>');
                }

                if( data["error"] != null ){
                    $(".alerts").html('<div class="alert status-error">'+data["error"]+'  .</div>'+'<div class="alert status-error">Reesayer encore</div>');
                }

                //affichage
                var tab = data.info;
                var html = "<table>";
                for (var i = 0; i < tab.length; i++) {
                    if (i == 0) {
                        html += "<thead>";
                        html += '<tr>';
                        for (var e = 0; e < tab[i].length; e++) {
                             html += '<th>'+tab[i][e]+'</th>';
                        }
                        html += '</tr>';
                        html += "</thead>";
                        html += "<tbody>";
                    }
                    else{
                        html += '<tr>';
                        for (var e = 0; e < tab[i].length; e++) {
                             html += '<td>'+tab[i][e]+'</td>';
                        }
                        html += '</tr>';
                    }
                    
                }
                html += "</tbody>";
                html += "</table>";

                $(".container_list").html(html);
            });
        });

        function exo_text(num){
            tab = [
                "Sélectionner toutes les personnes inscrites en affichant : <span class='syntax_col_word_key'>nom, prénom, mail, affiliation</span>",
                "Afficher les inscrits qui non pas accorder de droit image (droit_image=0)",
                "Afficher les événements dont identifiant est inférieur ou égal à 4",
                "Afficher les participants a les formations R, Python/Bash, requetage",
                "Afficher les participants ou id_evenement est compris entre [2, 4] (inclus)",
                "Afficher les participants à la formation R (id_evenement=2) et/ou Requetage (id_evenement=4)",
                "Afficher les femmes inscrites qui sont ingénieur (statut = ingénieur) et/ou les hommes inscrits qui sont étudiants",
                "Afficher les inscrits qui non pas indiqué leur affiliation (affiliation = NULL)",
                "Afficher les inscrits dont le mail ce termine par '.com'",
                "Afficher les inscrits dont le mail ce commence par 'mail2'",
                "Afficher les inscrits dont le mail contient les caractères 'in'",
                "Afficher les inscrits dont le mail contient les caractères 'ig' et un peu plus loin les caractères 'cn'",
                "Afficher les inscrits dont le mail contient les caractères '1' suivie d'un et unique caractère quelconque puis '@'",
                "Compter le nombre de participants qui participent à la formation alignement (id_evenement=6) : <span class='syntax_col_word_key'>nb_participant</span>",
                "Compter le nombre de participants pour chaque formation : <span class='syntax_col_word_key'>id_evenement, nb_participant</span>",
                "Compter le nombre de participants par ordre décroissant pour chaque formation : <span class='syntax_col_word_key'>id_evenement, nb_participant</span>",
                "Afficher l'evenement ou le nombre de participants est égal à 7 : <span class='syntax_col_word_key'>id_evenement, nb_participant</span>",
                "Afficher le nom, prénom, mail, id_evenement des participants : <span class='syntax_col_word_key'>nom, prénom, mail, id_evenement</span>",
                "Afficher les participants inscrits à la fois aux conférences (id_evenement=1) et à la formation alignement (id_evenement=6)",//10
                "Liste des personnes qui participent à la fois aux conférences et à la formation alignement, qui ont refusé les droits images : <span class='syntax_col_word_key'>nom, prénom, mail, droit_image</span>"];
            
            return tab[num-1];
        }


        function example_requete(num){
            tab = [
                "SELECT nom_du_champ1, nom_du_champ2 FROM nom_table<br>SELECT prenom, nom, ville FROM client",
                    "SELECT * FROM nom_table WHERE condition",
                    "SELECT * FROM nom_table WHERE condition",
                    "SELECT * FROM nom_table WHERE condition",
                    "SELECT * FROM nom_table WHERE condition_1 and condition_2",
                    "SELECT * FROM nom_table WHERE condition_1 or condition_2",
                    "SELECT * FROM nom_table WHERE (condition_1 AND condition_2) or (condition_3 AND condition_4)",
                    "SELECT * FROM table WHERE nom_colonne IS NULL",
                    "SELECT * FROM client WHERE ville LIKE '%llier'",
                    "SELECT * FROM client WHERE ville LIKE 'montp%'",
                    "SELECT * FROM client WHERE ville LIKE '%e%'",
                    "SELECT * FROM client WHERE ville LIKE '%e%'",
                    "SELECT * FROM client WHERE ville LIKE 'a_b'",
                    "SELECT COUNT(*) AS alias_colonne FROM table<br>SELECT COUNT(nom_colonne) FROM table",
                    "SELECT colonne1, fonction(colonne2) FROM table GROUP BY colonne1",
                    "SELECT colonne1, colonne2, colonne3 FROM table ORDER BY colonne1 DESC, colonne2 ASC",
                    "SELECT client, SUM(tarif) FROM achat GROUP BY client HAVING SUM(tarif) > 40",
                    "SELECT colonne1, colonne2 FROM nom_table_1, nom_table_2 WHERE value_tab1=value_tab2",
                    "SELECT * FROM nom_table, (SELECT colonne1 FROM nom_table) AS alias_table WHERE condition_1 or condition_2",
                    "SELECT value FROM `table1` WHERE condition and value IN (SELECT value FROM `table1` where condition);"];
            
            return tab[num-1];
        }

        function link_doc(num){
            tab = [
            "https://sql.sh/cours/select",
            "https://sql.sh/cours/where",
            "https://sql.sh/cours/where",
            "https://sql.sh/cours/where",
            "https://sql.sh/cours/where",
            "https://sql.sh/cours/where",
            "https://sql.sh/cours/where",
            "https://sql.sh/cours/where/is",
            "https://sql.sh/cours/where/like",
            "https://sql.sh/cours/where/like",
            "https://sql.sh/cours/where/like",
            "https://sql.sh/cours/where/like",
            "https://sql.sh/cours/where/like",
            "https://sql.sh/fonctions/agregation/count",
            "https://sql.sh/cours/group-by",
            "https://sql.sh/cours/order-by",
            "https://sql.sh/cours/having",
            "https://sql.sh/cours/jointures/cross-join",
            "https://sql.sh/cours/sous-requete",
            "https://sql.sh/cours/intersect"];
            
            return tab[num-1];
        }

        var max_exo = 20;

        $("#next_btn").on("click", function(){
            nb = parseInt($("#nb").val(), 10);
            if(nb + 1 <= max_exo){
                nb += 1;

                $("#nb").val(nb);
                $("#num_exo").html("EXO "+nb);
                $("#num_exo + p").html(exo_text(nb));
                $("#example_requete").html(example_requete(nb));
                $("#link_doc").attr("href", link_doc(nb));
                $("#link_doc").html(link_doc(nb));
            }
        });

        $("#pred_btn").on("click", function(){
            nb = parseInt($("#nb").val(), 10);
            if(nb - 1 >= 1){
                nb -= 1;

                $("#nb").val(nb);
                $("#num_exo").html("EXO "+nb);
                $("#num_exo + p").html(exo_text(nb));
                $("#example_requete").html(example_requete(nb));
                $("#link_doc").attr("href", link_doc(nb));
                $("#link_doc").html(link_doc(nb));
            }
        });

        $("#num_exo + p").html(exo_text(1));
        $("#link_doc").attr("href", link_doc(1));
        $("#example_requete").html(example_requete(1));

        $("#solution").on("click", function () {
            nb = parseInt($("#nb").val(), 10);
            $.post("php/query.php", {soluce:nb}, function(data) {
                data = JSON.parse(data);
                $("#query").val(data.reponse);
            });
        });

        function name_tab(mot){
            word = ["Inscrit", "Evenement", "Participe", "Inscrit,", "Evenement,", "Participe,", "Inscrit;", "Evenement;", "Participe;"];
            for (var i = 0; i < word.length; i++) {
                if(word[i].toUpperCase() == mot.toUpperCase()){
                    return {mot:word[i], bool:true};
                }
            }

            return {mot:mot, bool:false};
        }

        function word_key(mot){
            word = ["FROM", "SELECT", "(SELECT", "WHERE", "AND", "OR", "GROUP", "BY", "IN", "AS", "LIKE", "DESC", "ORDER", "NULL"];
            for (var i = 0; i < word.length; i++) {
                if(word[i].toUpperCase() == mot.toUpperCase()){
                    return true;
                }
            }

            return false;
        }

        function case_word_key() {
            text = $("#query").val();
            lignes = text.split("\n");
            lignes_color = text.split("\n");
            var cursorPosition = $('#query').prop("selectionStart");
            
            for (var e = 0; e < lignes.length; e++) {
                text_split_space = lignes[e].split(" ");
                text_split_space_color = lignes_color[e].split(" ");;

                for(i=0; i<text_split_space.length; i++){
                    if( word_key(text_split_space[i]) ){
                        text_split_space[i] = text_split_space[i].toUpperCase();
                        text_split_space_color[i] = "<span class='syntax_col_word_key'>"+text_split_space[i]+"</span>";

                    }
                    else{
                        var obj = name_tab(text_split_space[i]);
                        text_split_space[i] = obj.mot;
                        if(obj.bool){
                            text_split_space_color[i] = "<span class='syntax_col_table'>"+text_split_space[i]+"</span>";
                        }
                    }
                }
                lignes_color[e] = text_split_space_color.join(' ');
                lignes[e] = text_split_space.join(' ')
            }

            $("#sql").html(lignes_color.join('\n'));
            $("#query").val(lignes.join('\n'));

            $('#query').prop("selectionStart", cursorPosition);
            $('#query').prop("selectionEnd", cursorPosition);
        }

        $("#query").on("keyup", function (event) {
            if(event.which == 32 || event.which == 13){
                case_word_key();
            }
        });


    </script>

    <script type="text/javascript">
        choose_active("accueil.php");
    </script>

</body>
</html>
