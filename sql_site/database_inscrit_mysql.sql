DROP TABLE Participe;
DROP TABLE Evenement;
DROP TABLE Inscrit;
DROP TYPE statut_t;
DROP TYPE civilite_t;
DROP TYPE moment_t;
DROP TYPE type_t;


CREATE TABLE Evenement (
  ID INteger NOT NULL,
  nom varchar(355) NOT NULL,
  moment ENUM ('matin','aprem', 'journee'),
  type ENUM('conference','workshop') NOT NULL
);

--
-- Déchargement des données de la table Evenement
--

INSERT INTO Evenement (ID, nom, moment, type) VALUES
(1, 'conferences', 'journee', 'conference'),
(2, 'R', 'matin', 'workshop'),
(3, 'Python', 'matin', 'workshop'),
(4, 'Requetage', 'matin', 'workshop'),
(5, 'Phylogenie', 'aprem', 'workshop'),
(6, 'Alignement', 'aprem', 'workshop'),
(7, 'Machine_Learning', 'aprem', 'workshop');


-- --------------------------------------------------------

--
-- Structure de la table Inscrit
--

/*DROP TABLE participe;
DROP TABLE evenement;
DROP TABLE inscrit;*/

CREATE TABLE Inscrit (
  nom varchar(355) NOT NULL,
  prenom varchar(355) NOT NULL,
  mail varchar(100) NOT NULL,
  statut ENUM('etudiant','docteur','post-doctorant','enseignant','enseignant_chercheur','prive','ingenieur','autre',''),
  droit_image integer DEFAULT NULL,
  civilite ENUM('Monsieur','Madame','') DEFAULT NULL,
  abstract text NOT NULL,
  pays varchar(100),
  affiliation varchar(100)
);

/*ALTER TABLE Inscrit 
  MODIFY civilite enum('Monsieur','Madame','');*/

/*ALTER TABLE Inscrit 
  ADD pays VARCHAR (50) DEFAULT 'France',
  ADD affiliation VARCHAR (200) DEFAULT NULL;*/

--
-- Déchargement des données de la table Inscrit


-- --------------------------------------------------------

--
-- Structure de la table Participe
--

CREATE TABLE Participe (
  mail_inscrit varchar(100) NOT NULL,
  id_evenement INteger NOT NULL
);

--
-- Déchargement des données de la table Participe
--


--
-- Index pour les tables déchargées
--

--
-- Index pour la table Evenement
--
ALTER TABLE Evenement
  ADD PRIMARY KEY (ID);

--
-- Index pour la table Inscrit
--
ALTER TABLE Inscrit
  ADD PRIMARY KEY (mail);

--
-- Index pour la table Participe
--
ALTER TABLE Participe
  ADD PRIMARY KEY (mail_inscrit,id_evenement);

--
-- Contraintes pour la table Participe
--
ALTER TABLE Participe
  ADD CONSTRAINT Participe_ibfk_1 FOREIGN KEY (mail_inscrit) REFERENCES Inscrit (mail) ON DELETE CASCADE,
  ADD CONSTRAINT Participe_ibfk_2 FOREIGN KEY (id_evenement) REFERENCES Evenement (ID) ON DELETE RESTRICT;

COMMIT;
